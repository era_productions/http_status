# Changelog

## 1.0.1+1

- Moved `coverage` to `dev_dependencies` and downgraded to `^0.7.3`

## 1.0.1

- Added `HttpStatus.fromCode` constructor

## 1.0.0+3

- Increased coverage

## 1.0.0+2

- codecov.io setup

## 1.0.0+1

- Added dart_codecov_generator

## 1.0.0

- Created `HttpStatusCode` and `HttpStatus`

## 0.0.1

- Initial version, created by Stagehand
