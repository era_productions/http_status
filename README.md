# http_status

Http Status Codes for dart.

[![codecov](https://codecov.io/bb/era_productions/http_status/branch/master/graph/badge.svg)](https://codecov.io/bb/era_productions/http_status)
[ ![Codeship Status for era_productions/http_status](https://app.codeship.com/projects/e98c60a0-f7a6-0134-c2f5-62bad16a2d4d/status?branch=master)](https://app.codeship.com/projects/210773)

## Usage

A simple usage example:

    import 'package:http_status/http_status.dart';

    main() {
      print('${HttpStatusCode.OK}');
      print('${HttpStatus.OK}');
    }

## Features and bugs

Please file feature requests and bugs at the [issue tracker][tracker].

[tracker]: https://bitbucket.org/era_productions/http_status/issues
