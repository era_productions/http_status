// Copyright (c) 2017, Era Productions. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

import 'package:http_status/http_status.dart';

main() {
  print('${HttpStatusCode.OK}');
  print('${HttpStatus.OK}');
}
